extends CharacterBody2D


@export var SPEED = 100
@export var JUMP_VELOCITY = -200

# Get the gravity from the project settings to be synced with RigidBody nodes.
@export var gravity = 450
@export var respawn_point = Vector2(0,2)


func _physics_process(delta):
	# Add the gravity.
	if not is_on_floor():
		velocity.y += gravity * delta

	# Handle Jump.
	if Input.is_action_just_pressed("jump") and is_on_floor():
		velocity.y = JUMP_VELOCITY

	# Get the input direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	var direction = Input.get_axis("left", "right")
	if direction:
		velocity.x = direction * SPEED
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED)

	if Input.is_action_just_pressed("respawn"):
		position = respawn_point
	move_and_slide()


func _on_area_2d_body_entered(body):
	if "Spikes" in body.name:
		position = respawn_point

func _on_player_area_entered(area):
	if "checkpoint" in area.name:
		respawn_point = position
	elif "end" in area.name:
		print("DONE")
